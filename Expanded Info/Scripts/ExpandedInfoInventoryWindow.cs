using UnityEngine;
using DaggerfallWorkshop.Game.Items;
using DaggerfallWorkshop.Game.UserInterface;
using DaggerfallWorkshop.Game.UserInterfaceWindows;
using DaggerfallWorkshop.Game.Utility.ModSupport;
using DaggerfallWorkshop.Game.Questing;
using DaggerfallWorkshop.Utility;
using DaggerfallConnect.Utility;
using DaggerfallConnect.Arena2;

namespace DaggerfallWorkshop.Game.UserInterfaceWindows
{
    public class ExpandedInfoInventoryWindow : DaggerfallInventoryWindow
    {
        public ExpandedInfoInventoryWindow(IUserInterfaceManager uiManager, DaggerfallBaseWindow previous = null)
    : base(uiManager, previous)
        {

        }

        protected void ShowCustomInfoPopup(DaggerfallUnityItem item)
        {
            DaggerfallUI.Instance.PlayOneShot(SoundClips.ButtonClick);

            ExpandedInfoBox infoBox = new ExpandedInfoBox(uiManager, this);
            infoBox.DisplayInfoScreen(item);
            infoBox.Show();
           
        }
        
        protected override void AccessoryItemsButton_OnMouseClick(BaseScreenComponent sender, Vector2 position, ActionModes actionMode)
        {
            DaggerfallUI.Instance.PlayOneShot(SoundClips.ButtonClick);
            // Get item
            EquipSlots slot = (EquipSlots)sender.Tag;
            DaggerfallUnityItem item = playerEntity.ItemEquipTable.GetItem(slot);
            if (item == null)
                return;

            // Handle click based on action
            if (actionMode == ActionModes.Equip ||
                actionMode == ActionModes.Select)
            {
                UnequipItem(item);
            }
            else if (actionMode == ActionModes.Info)
            {
                ShowCustomInfoPopup(item);
            }
            else if (actionMode == ActionModes.Use)
            {
                UseItem(item);
            }
        }

        private DaggerfallUnityItem PaperDoll_GetItem(BaseScreenComponent sender, Vector2 position)
        {
            DaggerfallUI.Instance.PlayOneShot(SoundClips.ButtonClick);
            // Get equip value
            byte value = paperDoll.GetEquipIndex((int)position.x, (int)position.y);
            if (value == 0xff)
                return null;

            // Get item
            EquipSlots slot = (EquipSlots)value;
            DaggerfallUnityItem item = playerEntity.ItemEquipTable.GetItem(slot);
            return item;
        }

        protected override void PaperDoll_OnMouseClick(BaseScreenComponent sender, Vector2 position, ActionModes actionMode)
        {
            DaggerfallUnityItem item = PaperDoll_GetItem(sender, position);
            if (item == null)
                return;

            // Handle click based on action
            if (actionMode == ActionModes.Equip ||
                actionMode == ActionModes.Select)
            {
                UnequipItem(item);
            }
            else if (actionMode == ActionModes.Use)
            {
                UseItem(item);
            }
            else if (actionMode == ActionModes.Info)
            {
                ShowCustomInfoPopup(item);
            }
        }

        protected override void LocalItemListScroller_OnItemClick(DaggerfallUnityItem item, ActionModes actionMode)
        {
            // Handle click based on action
            if (actionMode == ActionModes.Equip)
            {
                if (item.IsLightSource)
                {
                    UseItem(item);
                    Refresh(false);
                }
                else
                    EquipItem(item);
            }
            else if (actionMode == ActionModes.Use)
            {
                // Allow item to handle its own use, fall through to general use function if unhandled
                if (!item.UseItem(localItems))
                    UseItem(item, localItems);
                Refresh(false);
            }
            else if (actionMode == ActionModes.Remove)
            {
                // Transfer to remote items
                if (remoteItems != null && !chooseOne)
                {
                    int? canHold = null;
                    if (usingWagon)
                        canHold = WagonCanHoldAmount(item);
                    TransferItem(item, localItems, remoteItems, canHold, true);
                    if (theftBasket != null && lootTarget != null && lootTarget.houseOwned)
                        theftBasket.RemoveItem(item);
                }
            }
            else if (actionMode == ActionModes.Info)
            {
                ShowCustomInfoPopup(item);
            }
        }

        protected override void RemoteItemListScroller_OnItemClick(DaggerfallUnityItem item, ActionModes actionMode)
        {
            // Send click to quest system
            if (item.IsQuestItem)
            {
                Quest quest = QuestMachine.Instance.GetQuest(item.QuestUID);
                if (quest != null)
                {
                    Item questItem = quest.GetItem(item.QuestItemSymbol);
                    if (quest != null)
                        questItem.SetPlayerClicked();
                }
            }

            // Handle click based on action
            if (actionMode == ActionModes.Equip)
            {
                // Transfer to local items
                if (localItems != null)
                    TransferItem(item, remoteItems, localItems, CanCarryAmount(item), equip: true);
                if (theftBasket != null && lootTarget != null && lootTarget.houseOwned)
                    theftBasket.AddItem(item);
            }
            else if (actionMode == ActionModes.Use)
            {
                // Allow item to handle its own use, fall through to general use function if unhandled
                if (!item.UseItem(remoteItems))
                    UseItem(item, remoteItems);
                Refresh(false);
            }
            else if (actionMode == ActionModes.Remove)
            {
                TransferItem(item, remoteItems, localItems, CanCarryAmount(item));
                if (theftBasket != null && lootTarget != null && lootTarget.houseOwned)
                    theftBasket.AddItem(item);
            }
            else if (actionMode == ActionModes.Info)
            {
                ShowCustomInfoPopup(item);
            }
        }
        
    }
}
