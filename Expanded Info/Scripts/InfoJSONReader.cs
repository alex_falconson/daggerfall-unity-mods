using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using DaggerfallWorkshop.Game.Items;
using Newtonsoft.Json;


namespace DaggerfallWorkshop.Utility.AssetInjection
{
    public class InfoJSONReader : MonoBehaviour
    {
        Dictionary<string, Dictionary<string, Dictionary<string, string>>> itemDescriptions = new Dictionary<string, Dictionary<string, Dictionary<string, string>>>();

        // Start is called before the first frame update
        public void Setup(string[] fileNames)
        {
            foreach (string file in fileNames) {
                string jsonString = File.ReadAllText(file);
                try
                {
                    Dictionary<string, Dictionary<string, Dictionary<string, string>>> temp = JsonConvert.DeserializeObject<Dictionary<string, Dictionary<string, Dictionary<string, string>>>>(jsonString);
                    AddToDictionary(temp);
                }
                catch
                {
                    Debug.Log("Expanded Info: could not read file: " + file + "\nCheck that the json is valid and organized like so: {group:{index:{key:value}}}.");
                }
                
            }
        }

        public void AddToDictionary(Dictionary<string, Dictionary<string, Dictionary<string, string>>> newJson) {
            foreach (string group in newJson.Keys)
            {
                if (itemDescriptions.ContainsKey(group))
                {
                    foreach (string index in newJson[group].Keys)
                    {
                        if (itemDescriptions[group].ContainsKey(index))
                        {
                            foreach(string key in newJson[group][index].Keys)
                            {
                                if (itemDescriptions[group][index].ContainsKey(key))
                                {
                                    itemDescriptions[group][index][key] = newJson[group][index][key];
                                }
                                else
                                {
                                    itemDescriptions[group][index].Add(key, newJson[group][index][key]);
                                }
                            }
                        }
                        else
                        {
                            itemDescriptions[group].Add(index, newJson[group][index]);
                        }
                    }
                }
                else
                {
                    itemDescriptions.Add(group, newJson[group]);
                }
            }
        }

        public string GetShortDescription(DaggerfallUnityItem item)
        {
            Debug.Log("Expanded Info: looking up item with group " + item.ItemGroup.ToString() + " and index " + item.TemplateIndex.ToString());
            string temp = "";
            if (itemDescriptions.ContainsKey("default"))
            {
                if (itemDescriptions["default"].ContainsKey("default"))
                {
                    if (itemDescriptions["default"]["default"].ContainsKey("shortDescription"))
                    {
                        temp = itemDescriptions["default"]["default"]["shortDescription"];
                    }
                }
            }
            string group = item.ItemGroup.ToString();
            if (itemDescriptions.ContainsKey(group))
            {
                if (itemDescriptions[group].ContainsKey("default"))
                {
                    if (itemDescriptions[group]["default"].ContainsKey("shortDescription"))
                    {
                        temp = itemDescriptions[group]["default"]["shortDescription"];
                    }
                }
                string index = item.TemplateIndex.ToString();
                if (itemDescriptions[group].ContainsKey(index))
                {
                    if (itemDescriptions[group][index].ContainsKey("shortDescription"))
                    {
                        temp = itemDescriptions[group][index]["shortDescription"];
                    }
                }
            }

            return temp;
        }

        public string GetLongDescription(DaggerfallUnityItem item)
        {
            string temp = "";
            if (itemDescriptions.ContainsKey("default"))
            {
                if (itemDescriptions["default"].ContainsKey("default"))
                {
                    if (itemDescriptions["default"]["default"].ContainsKey("longDescription"))
                    {
                        temp = itemDescriptions["default"]["default"]["longDescription"];
                    }
                }
            }
            string group = item.ItemGroup.ToString();
            if (itemDescriptions.ContainsKey(group))
            {
                if (itemDescriptions[group].ContainsKey("default"))
                {
                    if (itemDescriptions[group]["default"].ContainsKey("longDescription"))
                    {
                        temp = itemDescriptions[group]["default"]["longDescription"];
                    }
                }
                string index = item.TemplateIndex.ToString();
                if (itemDescriptions[group].ContainsKey(index))
                {
                    if (itemDescriptions[group][index].ContainsKey("longDescription"))
                    {
                        temp = itemDescriptions[group][index]["longDescription"];
                    }
                }
            }

            return temp;
        }

    }
}
