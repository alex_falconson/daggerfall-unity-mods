using UnityEngine;
using DaggerfallWorkshop.Game.Banking;
using DaggerfallWorkshop.Game.Entity;
using DaggerfallWorkshop.Game.Formulas;
using DaggerfallWorkshop.Game.Guilds;
using DaggerfallWorkshop.Game.Items;
using DaggerfallWorkshop.Game.UserInterface;
using DaggerfallWorkshop.Game.Utility;
using DaggerfallWorkshop.Utility;
using ExpandedInfoModHook;

namespace DaggerfallWorkshop.Game.UserInterfaceWindows
{
    public class ExpandedInfoTradeWindow : DaggerfallTradeWindow
    {
        const int doesNotNeedToBeRepairedTextId = 24;
        const int magicItemsCannotBeRepairedTextId = 33;

        DaggerfallUnityItem itemBeingRepaired;

        public ExpandedInfoTradeWindow(IUserInterfaceManager uiManager, DaggerfallBaseWindow previous = null, WindowModes windowMode = WindowModes.Sell, IGuild guild = null)
            : base(uiManager, previous, windowMode, guild)
        {
        }

        #region Item Click Event Handlers

        protected override void LocalItemListScroller_OnItemClick(DaggerfallUnityItem item, ActionModes actionMode)
        {
            // Handle click based on action & mode
            if (actionMode == ActionModes.Select || actionMode == ActionModes.Remove)
            {
                switch (WindowMode)
                {
                    case WindowModes.Sell:
                    case WindowModes.SellMagic:
                        if (remoteItems != null)
                        {
                            // Are we trying to sell the non empty wagon?
                            if (item.ItemGroup == ItemGroups.Transportation && PlayerEntity.WagonItems.Count > 0)
                            {
                                DaggerfallUnityItem usedWagon = PlayerEntity.Items.GetItem(ItemGroups.Transportation, (int)Transportation.Small_cart);
                                if (usedWagon.Equals(item))
                                    return;
                            }
                            TransferItem(item, localItems, remoteItems);
                        }
                        break;

                    case WindowModes.Buy:
                        if (UsingWagon)                             // Allows player to get & equip stuff from cart while purchasing.
                            TransferItem(item, localItems, PlayerEntity.Items, CanCarryAmount(item), equip: !item.IsAStack());
                        else if (actionMode == ActionModes.Remove && basketItems.Contains(item))    // Allows clearing individual items
                            TransferItem(item, basketItems, remoteItems);
                        else if (actionMode == ActionModes.Select)  // Allows player to equip and unequip while purchasing.
                            EquipItem(item);
                        break;

                    case WindowModes.Repair:
                        // Check that item can be repaired, is damaged & transfer if so.
                        if (item.IsEnchanted && !DaggerfallUnity.Settings.AllowMagicRepairs)
                            DaggerfallUI.MessageBox(magicItemsCannotBeRepairedTextId);
                        else if (item.ItemTemplate.isNotRepairable)
                            DaggerfallUI.MessageBox(TextManager.Instance.GetLocalizedText("cannotBeRepaired"));
                        else if ((item.currentCondition == item.maxCondition))
                            DaggerfallUI.MessageBox(doesNotNeedToBeRepairedTextId);
                        else
                            TransferItem(item, localItems, remoteItems);
                        break;

                    case WindowModes.Identify:
                        // Check if item is unidentified & transfer
                        // In spell mode items can be transferred even if identified (matches classic)
                        if (!item.IsIdentified || UsingIdentifySpell)
                            TransferItem(item, localItems, remoteItems);
                        else
                            DaggerfallUI.MessageBox(TextManager.Instance.GetLocalizedText("doesntNeedIdentify"));
                        break;
                }
            }
            else if (actionMode == ActionModes.Info)
            {
                ShowExpandedInfoPopup(item);
            }
        }

        protected override void RemoteItemListScroller_OnItemClick(DaggerfallUnityItem item, ActionModes actionMode)
        {
            // Handle click based on action
            if (actionMode == ActionModes.Select || actionMode == ActionModes.Remove)
            {
                if (WindowMode == WindowModes.Buy)
                    TransferItem(item, remoteItems, basketItems, CanCarryAmount(item), equip: !item.IsAStack() && actionMode == ActionModes.Select);
                else if (WindowMode == WindowModes.Repair)
                {
                    if (item.RepairData.IsBeingRepaired() && !item.RepairData.IsRepairFinished())
                    {
                        itemBeingRepaired = item;
                        string strInterruptRepair = TextManager.Instance.GetLocalizedText("interruptRepair");
                        DaggerfallMessageBox confirmInterruptRepairBox = new DaggerfallMessageBox(uiManager, DaggerfallMessageBox.CommonMessageBoxButtons.YesNo, strInterruptRepair, this);
                        confirmInterruptRepairBox.OnButtonClick += ConfirmInterruptRepairBox_OnButtonClick;
                        confirmInterruptRepairBox.Show();
                    }
                    else
                        TakeItemFromRepair(item);
                }
                else
                    TransferItem(item, remoteItems, localItems, UsingWagon ? WagonCanHoldAmount(item) : CanCarryAmount(item), blockTransport: UsingWagon);
            }
            else if (actionMode == ActionModes.Info)
            {
                ShowExpandedInfoPopup(item);
            }
        }

        private void ConfirmInterruptRepairBox_OnButtonClick(DaggerfallMessageBox sender, DaggerfallMessageBox.MessageBoxButtons messageBoxButton)
        {
            sender.CloseWindow();
            if (messageBoxButton == DaggerfallMessageBox.MessageBoxButtons.Yes)
            {
                TakeItemFromRepair(itemBeingRepaired);
            }
        }

        private void TakeItemFromRepair(DaggerfallUnityItem item)
        {
            TransferItem(item, remoteItems, localItems, UsingWagon ? WagonCanHoldAmount(item) : CanCarryAmount(item));
            item.RepairData.Collect();
            // UpdateRepairTimes(false);
        }

        #endregion


        protected void ShowExpandedInfoPopup(DaggerfallUnityItem item)
        {
            DaggerfallUI.Instance.PlayOneShot(SoundClips.ButtonClick);

            ExpandedInfoBox infoBox = new ExpandedInfoBox(uiManager, this);
            infoBox.DisplayInfoScreen(item);
            infoBox.Show();
           
        }
    }

}
