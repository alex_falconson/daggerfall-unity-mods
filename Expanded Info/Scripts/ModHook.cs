using UnityEngine;
using System;
using System.IO;
using DaggerfallWorkshop.Game;
using DaggerfallWorkshop.Game.Utility.ModSupport;
using DaggerfallWorkshop.Game.UserInterface;
using DaggerfallWorkshop.Game.UserInterfaceWindows;
using DaggerfallWorkshop.Utility.AssetInjection;

namespace ExpandedInfoModHook
{
    public class ExpandedInfo : MonoBehaviour
    {
        private static Mod mod;
        public static InfoJSONReader ExpandedInfoJsonReader;

        string textFolderName = "Text";
        //UserInterfaceManager uiManager = new UserInterfaceManager();
        //ExpandedInfoInventoryWindow UIWindow;

        [Invoke(StateManager.StateTypes.Start, 0)]
        public static void Init(InitParams initParams)
        {
            mod = initParams.Mod;

            var go = new GameObject(mod.Title);
            go.AddComponent<ExpandedInfo>();

            mod.IsReady = true;
        }

        private void Start()
        {
            Debug.Log("ExpandedInfo: mod loaded");

            UIWindowFactory.RegisterCustomUIWindow(UIWindowType.Inventory, typeof(ExpandedInfoInventoryWindow));
            UIWindowFactory.RegisterCustomUIWindow(UIWindowType.Trade, typeof(ExpandedInfoTradeWindow));

            ExpandedInfoJsonReader = new InfoJSONReader();

            string path = Path.Combine(Application.streamingAssetsPath, textFolderName);
            string[] allFiles = Directory.GetFiles(path, "ExpandedInfo*.json");

            ExpandedInfoJsonReader.Setup(allFiles);

            //UIWindow = (ExpandedInfoInventoryWindow)UIWindowFactory.GetInstance(UIWindowType.Inventory, uiManager, null);

            //  InfoJSONReader reader = new InfoJSONReader();

        }
    }
}