using System;
using System.IO;
using System.Collections.Generic;
using UnityEngine;
using ExpandedInfoModHook;
using DaggerfallWorkshop.Game;
using DaggerfallWorkshop.Game.Items;
using DaggerfallWorkshop.Game.UserInterface;
using DaggerfallWorkshop.Game.UserInterfaceWindows;
using DaggerfallWorkshop.Localization;
using DaggerfallWorkshop.Utility;
using DaggerfallConnect.Arena2;

namespace DaggerfallWorkshop.Game.UserInterfaceWindows
{
    public class ExpandedInfoBox : DaggerfallPopupWindow
    {
        Panel descriptionPanel = new Panel();
        Panel imagePanel = new Panel();
        Panel namePanel = new Panel();
        Panel infoPanel = new Panel();
        Panel pagePanel = new Panel();

        const string nativeImgName = "BOOK00I0.IMG";

        Texture2D nativeTexture;
        const int shiftMarginVertical = 10;
        const int shiftMarginHorizontal = 20;
        const float scrollAmount = 24;
        float scrollPosition = 0;
        float maxHeight = 0;

        static Vector2 pagePanelPosition = new Vector2(15, 22);
        static Vector2 pagePanelSize = new Vector2(290, 157);

        static Vector2 namePanelPosition = new Vector2(0, 0);
        static Vector2 namePanelSize = new Vector2(100, 20);

        static Vector2 imagePanelPosition = new Vector2(namePanelPosition[0], namePanelPosition[1] + namePanelSize[1] + shiftMarginVertical);
        static Vector2 imagePanelSize = new Vector2(100, 67);

        static Vector2 infoPanelPosition = new Vector2 (imagePanelPosition[0], imagePanelPosition[1] + imagePanelSize[1] + shiftMarginVertical);
        static Vector2 infoPanelSize = new Vector2(100, 50);

        static Vector2 descriptionPanelPosition = new Vector2(namePanelPosition[0] + namePanelSize[0] + shiftMarginHorizontal, namePanelPosition[1]);
        static Vector2 descriptionPanelSize = new Vector2(170, 157);

        List<TextLabel> descriptionLabels = new List<TextLabel>();
        List<Panel> descriptionImages = new List<Panel>();

        public ExpandedInfoBox(IUserInterfaceManager uiManager, IUserInterfaceWindow previous = null)
        : base(uiManager, previous)
        {

        }

        protected override void Setup()
        {
            if (IsSetup)
                return;

            nativeTexture = DaggerfallUI.GetTextureFromImg(nativeImgName);
            if (!nativeTexture)
                throw new Exception("ExpandedInfoBox: Could not load native texture.");

            // Setup native panel background
            NativePanel.BackgroundTexture = nativeTexture;

            pagePanel.Position = pagePanelPosition;
            pagePanel.Size = pagePanelSize;

            NativePanel.Components.Add(pagePanel);

            namePanel.Position = namePanelPosition;
            namePanel.Size = namePanelSize;
            pagePanel.Components.Add(namePanel);

            /*
            nameLabel.HorizontalAlignment = HorizontalAlignment.Center;
            nameLabel.VerticalAlignment = VerticalAlignment.Middle;
            namePanel.Components.Add(nameLabel);
            */
            imagePanel.Position = imagePanelPosition;
            imagePanel.Size = imagePanelSize;
            imagePanel.MaxAutoScale = 1f;
            pagePanel.Components.Add(imagePanel);
            /*
            iconPanel = DaggerfallUI.AddPanel(imagePanel, AutoSizeModes.ScaleToFit);
            iconPanel.HorizontalAlignment = HorizontalAlignment.Center;
            iconPanel.VerticalAlignment = VerticalAlignment.Middle;
            */
            infoPanel.Position = infoPanelPosition;
            infoPanel.Size = infoPanelSize;
            pagePanel.Components.Add(infoPanel);
            /*
            infoLabel.HorizontalAlignment = HorizontalAlignment.Center;
            infoLabel.VerticalAlignment = VerticalAlignment.Middle;
            infoPanel.Components.Add(infoLabel);
            */
            descriptionPanel.Position = descriptionPanelPosition;
            descriptionPanel.Size = descriptionPanelSize;
            descriptionPanel.OnMouseScrollUp += DescriptionPanel_OnMouseScrollUp;
            descriptionPanel.OnMouseScrollDown += DescriptionPanel_OnMouseScrollDown;
            descriptionPanel.RectRestrictedRenderArea = new Rect(descriptionPanel.Position, descriptionPanel.Size);
            pagePanel.Components.Add(descriptionPanel);

             // Add buttons
            Button nextPageButton = DaggerfallUI.AddButton(new Rect(208, 188, 14, 8), NativePanel);
            nextPageButton.OnMouseClick += NextPageButton_OnMouseClick;

            Button previousPageButton = DaggerfallUI.AddButton(new Rect(181, 188, 14, 48), NativePanel);
            previousPageButton.OnMouseClick += PreviousPageButton_OnMouseClick;

            Button exitButton = DaggerfallUI.AddButton(new Rect(277, 187, 32, 10), NativePanel);
            exitButton.OnMouseClick += ExitButton_OnMouseClick;

            IsSetup = true;

        }

        public void DisplayInfoScreen (DaggerfallUnityItem item)
        {
            if (!IsSetup)
                Setup();

            // Display left panels
            DisplayInfo(item);

            // Display description
            DisplayDescription(item);
        }

        void DisplayInfo (DaggerfallUnityItem item)
        {
            // Resolve name
            string itemName = item.LongName;
            TextLabel nameLabel = CreateLabel(DaggerfallUI.Instance.Font1, HorizontalAlignment.Center, DaggerfallUI.DaggerfallDefaultTextColor, itemName);
            nameLabel.MaxWidth = (int)namePanelSize.x;
            namePanel.Components.Add(nameLabel);


            // Resolve image
            ImageData image;
            Panel iconPanel = DaggerfallUI.AddPanel(imagePanel, AutoSizeModes.ScaleToFit);
            iconPanel.HorizontalAlignment = HorizontalAlignment.Center;
            iconPanel.VerticalAlignment = VerticalAlignment.Middle;
            if (item.ItemGroup == ItemGroups.Paintings)
                image = ImageReader.GetImageData(item.GetPaintingFilename(), item.GetPaintingFileIdx(), 0, true, true);
            else
                image = DaggerfallUnity.Instance.ItemHelper.GetInventoryImage(item);

            if (image.animatedTextures != null && image.animatedTextures.Length > 0)
                iconPanel.AnimatedBackgroundTextures = image.animatedTextures;
            iconPanel.BackgroundTexture = image.texture;
            if (image.width != 0 && image.height != 0)
                iconPanel.Size = new Vector2(image.width, image.height);
            else
                iconPanel.Size = new Vector2(image.texture.width, image.texture.height);


            // Resolve info
            TextFile.Token[] tokens = ItemHelper.GetItemInfo(item, DaggerfallUnity.TextProvider);

            if (item.ItemGroup == ItemGroups.Paintings) {
                List<TextFile.Token> tokenList = new List<TextFile.Token>(tokens);
                tokenList.Add(TextFile.CreateFormatToken(TextFile.Formatting.JustifyCenter));
                tokenList.Add(TextFile.CreateTextToken("Weight: %kg kilograms"));
                tokenList.Add(TextFile.CreateFormatToken(TextFile.Formatting.JustifyCenter));
                tokens = tokenList.ToArray();
            }

            MacroHelper.ExpandMacros(ref tokens, item);

            if (!item.IsPotionRecipe && item.ItemGroup != ItemGroups.Paintings) {
                List<TextFile.Token> tokenList = new List<TextFile.Token>(tokens);
                tokenList.Remove(tokenList.Find(r => r.text.Contains(itemName)));
                tokens = tokenList.ToArray();
            }

            MultiFormatTextLabel infoLabel = new MultiFormatTextLabel();
            infoLabel.HorizontalAlignment = HorizontalAlignment.Center;
            infoLabel.SetText(tokens);
            infoPanel.Components.Add(infoLabel);
        }

        void DisplayDescription (DaggerfallUnityItem item)
        {
            List<TextLabel> labels;

            string shortDescription = ExpandedInfo.ExpandedInfoJsonReader.GetShortDescription(item);
            if (shortDescription != "")
            {
                labels = CreateLabels(DaggerfallStringTableImporter.ConvertStringToRSCTokens(shortDescription));
                descriptionLabels.AddRange(labels);
                foreach(TextLabel label in labels)
                        descriptionPanel.Components.Add(label);
                descriptionLabels.Add(CreateLabel(DaggerfallUI.DefaultFont, HorizontalAlignment.Left, DaggerfallUI.DaggerfallDefaultTextColor, string.Empty));
            }
            
            
            if (item.IsPotionRecipe)
                {
                    TextFile.Token[] tokens;
                    tokens = item.GetMacroDataSource().PotionRecipeIngredients(TextFile.Formatting.JustifyCenter);
                    labels = CreateLabels(tokens, true);
                    descriptionLabels.AddRange(labels);
                    foreach(TextLabel label in labels)
                        descriptionPanel.Components.Add(label);

                }
            else if (item.legacyMagic != null)
                {
                    TextFile.Token[] tokens;
                    tokens = DaggerfallUnity.Instance.TextProvider.GetRSCTokens(1016);
                    MacroHelper.ExpandMacros(ref tokens, item);
                    labels = CreateLabels(tokens, true);
                    //LayoutLabels(labels, (int)descriptionPanel.Size.x, descriptionPanel.RectRestrictedRenderArea);
                    descriptionLabels.AddRange(labels);
                    foreach(TextLabel label in labels)
                        descriptionPanel.Components.Add(label);
                }

            string longDescription = ExpandedInfo.ExpandedInfoJsonReader.GetLongDescription(item);
            if (longDescription != "")
            {
                labels = CreateLabels(DaggerfallStringTableImporter.ConvertStringToRSCTokens(longDescription));
                descriptionLabels.AddRange(labels);
                foreach(TextLabel label in labels)
                        descriptionPanel.Components.Add(label);
            }

            LayoutLabels(descriptionLabels, (int)descriptionPanel.Size.x, descriptionPanel.RectRestrictedRenderArea);
        }

        void LayoutLabels(List<TextLabel> labels, int width, Rect restrictedArea)
        {
            foreach (TextLabel label in labels)
                {
                    label.Position = new Vector2(0, maxHeight);
                    label.MaxWidth = width;
                    label.RectRestrictedRenderArea = restrictedArea;
                    label.RestrictedRenderAreaCoordinateType = TextLabel.RestrictedRenderArea_CoordinateType.ParentCoordinates;
                    maxHeight += label.Size.y;
                }
        }

        List<TextLabel> CreateLabels(TextFile.Token[] tokens, bool overrideFormatting = false)
        {
            List<TextLabel> labels = new List<TextLabel>();
            DaggerfallFont currentFont = DaggerfallUI.DefaultFont;
            HorizontalAlignment currentAlignment = HorizontalAlignment.Left;
            Color currentColor = DaggerfallUI.DaggerfallDefaultTextColor;
            float currentScale = 1.0f;

            if (tokens == null || tokens.Length == 0)
                {
                    // Empty or newline label - also resets alignment, color, scale
                    labels.Add(CreateLabel(DaggerfallUI.DefaultFont, HorizontalAlignment.Left, DaggerfallUI.DaggerfallDefaultTextColor, string.Empty));
                    currentAlignment = HorizontalAlignment.Left;
                    currentColor = DaggerfallUI.DaggerfallDefaultTextColor;
                    currentScale = 1.0f;
                }
            else
            {
                foreach (TextFile.Token token in tokens)
                {
                    switch (token.formatting)
                    {
                        case TextFile.Formatting.FontPrefix:
                            if (!overrideFormatting)
                                currentFont = DaggerfallUI.Instance.GetFont((DaggerfallFont.FontName)token.x - 1);
                            break;
                        case TextFile.Formatting.JustifyLeft:
                            if (!overrideFormatting)
                                currentAlignment = HorizontalAlignment.Left;
                            break;
                        case TextFile.Formatting.JustifyCenter:
                            if (!overrideFormatting)
                                currentAlignment = HorizontalAlignment.Center;
                            break;
                        /*
                        case TextFile.Formatting.Color:
                            currentColor = TryParseColor(token.text);
                            break;
                        case TextFile.Formatting.Scale:
                            currentScale = TryParseScale(token.text);
                            break;
                        case TextFile.Formatting.Image:
                            labels.Add(CreateImageLabel(token.text));
                            break;*/
                        default:
                            labels.Add(CreateLabel(currentFont, currentAlignment, currentColor, token.text, currentScale));
                            break;
                    }
                        
                }
            }

            return labels;
        }

        TextLabel CreateLabel(DaggerfallFont font, HorizontalAlignment alignment, Color color, string text, float scale = 1.0f)
        {
            // Every group is cast into a word-wrapping label
            TextLabel label = new TextLabel();
            label.Font = font;
            label.HorizontalAlignment = alignment;
            label.TextColor = color;
            label.ShadowColor = DaggerfallUI.DaggerfallDefaultShadowColor;
            label.ShadowPosition = DaggerfallUI.DaggerfallDefaultShadowPos;
            label.WrapText = true;
            label.WrapWords = true;
            label.Text = text;
            label.TextScale = scale;
            if (label.HorizontalAlignment == HorizontalAlignment.Center)
                label.HorizontalTextAlignment = TextLabel.HorizontalTextAlignmentSetting.Center;

            return label;
        }

        public void Show()
        {
            if (!IsSetup)
                Setup();

            uiManager.PushWindow(this);
        }

        private void NextPageButton_OnMouseClick(BaseScreenComponent sender, Vector2 position)
        {
            ScrollDescription(-scrollAmount);
        }

        private void PreviousPageButton_OnMouseClick(BaseScreenComponent sender, Vector2 position)
        {
            ScrollDescription(scrollAmount);
        }

        private void DescriptionPanel_OnMouseScrollUp(BaseScreenComponent sender)
        {
            ScrollDescription(scrollAmount);
        }

        private void DescriptionPanel_OnMouseScrollDown(BaseScreenComponent sender)
        {
            ScrollDescription(-scrollAmount);
        }

        private void ExitButton_OnMouseClick(BaseScreenComponent sender, Vector2 position)
        {
            DaggerfallUI.Instance.PlayOneShot(SoundClips.ButtonClick);
            CloseWindow();
        }


        void ScrollDescription(float amount)
        {

            // Stop scrolling at top or bottom of book layout
            if (amount < 0 && scrollPosition - pagePanel.Size.y - amount < -maxHeight)
                return;
            else if (amount > 0 && scrollPosition == 0)
                return;

            // Scroll label and only draw what can be seen
            scrollPosition += amount;
            foreach (TextLabel label in descriptionLabels)
            {
                label.Position = new Vector2(label.Position.x, label.Position.y + amount);
            }
        }
    }
}

